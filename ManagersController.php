<?php
/**
 *  Контроллер ManagersController обрабатывает действия связанные с сущностью "Manager"
 */


namespace App\Controllers;

use Validator;
use DB;
use Illuminate\Http\Request;
use yajra\Datatables\Datatables;
use Carbon\Carbon;

use Extend\Controller\AjaxResponseTrait;
use Extend\Controller\BaseController;

use App\Models\Mail\Account;
use App\Models\Site\Chat\Manager;
use App\Models\Site\Chat;
use App\Models\Site\Site;
use App\Models\Site\Chat\ManagerRelation;
use App\Models\Site\Chat\ChatSeat;
use App\Models\Payment\Payment;
use App\Models\Site\Agent\Integration;

use App\Exceptions\ManagerStatusException;

class ManagersController extends BaseController
{
    use AjaxResponseTrait;

    /**
     * Отдаёт view со списком менеджеров
     */
    public function managersList()
    {
        $user = $this->user;

        $sites = $user->sites()->with(['chat', 'callback'])->get();

        return $this->respond(view('user.managers.index', compact('sites','user')));
    }

    /**
     * Отдаёт view со списком операторов для модуля
     */
    public function operatorsList(Request $request, $module, $siteId)
    {
        $site = $this->user->sites()->findOrFail($siteId);

        $moduleInstance = null;

        if (in_array($module, $site->getAgentSite())){
            $moduleInstance = $site;
        } elseif (!empty($site->$module)) {
            $moduleInstance = $site->$module;
        }

        if (!($moduleInstance instanceof \App\Models\Site\ManagerableInterface)) {
            throw new \Exception("Module $module does not support managers");
        }

        $params = collect([
            'site' => $site,
            'module' => $module
        ])->merge(
            $request->only(['region_enabled', 'url_enabled', 'additional_number_enabled', 'settings'])
        );

        $view = "user.sites.{$module}.section._operators-list";

        if ($module === \App\Models\Site\Widget::MODULE_NAME) {
            if (!$site->siteManagers($module)->count()) {
                $view = "user.sites.{$module}.section._manager_template_first";
            }
        }

        if(in_array($module,$site->getAgentSite())){
            $view = "user.sites.agent.section._operators-list";
        }

        return $this->respond(
            view($view, $params)
        );
    }

    /**
     * Отдаёт view для модального окна добавления менеджера
     */
    public function modalCreate(Request $request, $siteId, $module)
    {
        $manager = new Manager;

        $site = $siteId ? $this->user->sites()->findOrFail($siteId) : null;

        return $this->respond(view('user.managers.modal.edit', compact('manager','siteId','module','site')));
    }

    /**
     * Отдаёт view для упрощенной формы модалки добавления менеджера
     */
    public function modalShortCreate(Request $request)
    {
        $manager = new Manager;

        // допустимо отсутствие siteId
        $site = $this->user->sites()->find($request->get('siteId'));

        return $this->respond(
            view('user.managers.modal.short-edit', compact('manager', 'site'))
        );
    }


    /**
     * Отдаёт view для модального окна редактирования менеджера
     * (сокращённая форма окна, для редактирования в настройках коллбэка)
     */
    public function modalShortUpdate($managerId, Request $request)
    {
        $site = $this->user->sites()->findOrFail($request->get('siteId'));
        $manager = $site->siteManagers()->findOrFail($managerId);

        return $this->respond(view('user.managers.modal.short-edit', compact('manager')));
    }

    /**
     * Отдаёт view для модального окна удаления менеджера
     */
    public function modalDelete($managerId)
    {
        $manager = $this->user->userManagers()->findOrFail($managerId);
        $sites = $this->user->sites;

        foreach ($sites as $site) {
            $result = $site->siteManagers(\App\Models\Site\Widget::MODULE_NAME);

            if ($result->count() === 1 and $result->first()->id === $manager->id) {
                return $this->respondValidation([
                    'error' => "В проекте {$site->url} это единственный менеджер, поэтому его нельзя удалить."
                ]);
            }

        }

        return $this->respond(view('user.managers.modal.delete', compact('manager')));
    }


    /**
     * Отдаёт view для модального окна редактирования алиаса менеджера
     * (алиас - это аватарка и имя менеджера для конкретного чата)
     */
    public function modalUpdateAlias($managerId, $siteId)
    {
        $manager = $this->user->userManagers()->findOrFail($managerId);

        $site = $manager->sites()->findOrFail($siteId);

        return $this->respond(view('user.managers.modal.edit-alias', compact('manager', 'site')));
    }

    /**
     * Модалка для подтверждения оплаты менеджера чата
     */
    public function modalPaymentConfirm()
    {
        return $this->respond(view('user.managers.modal.chat-payment-confirm'));
    }

    /**
     * Модалка со списком всех менеджеров для модуля
     */

    public function modalAllManagers($module, $siteId)
    {
        $site = $this->user->sites()->findOrFail($siteId);

        $moduleInstance = null;

        if (in_array($module, $site->getAgentSite())) {
            $moduleInstance = $site;
        } elseif (!empty($site->$module)) {
            $moduleInstance = $site->$module;
        }

        if (!($moduleInstance instanceof \App\Models\Site\ManagerableInterface)) {
            throw new \Exception("Module $module does not support managers");
        }

        // $columns = get_class($moduleInstance)::getAllManagersModalColumns() -- говорят не работает в PHP < 7, поверю на слово
        $columns = forward_static_call([get_class($moduleInstance), 'getAllManagersModalColumns']);

        $modalContent = view('user.managers.modal.all-managers', compact('site','columns'))->render();
        $modalButtons = view('user.managers.modal._select_operator_modal_buttons', compact('module','site'))->render();
        $modalTitle = "Выберите или создайте нового менеджера";

        return $this->respond(
            view('common.modal-dialog', 
                compact('modalContent', 'modalTitle', 'modalButtons')
            )
        );
    }



    /**
     * Модалка покупки мест для менеджеров чата
     */
    public function modalBuyChatSeats(Request $request)
    {
        $siteId = $request->get('siteId');
        $mode = $request->get('mode');
        $operatorNo = $request->get('operatorNo');
        // префикс для строк в select'e выбора количества операторов
        $countSelectPrefix = $mode === 'seats' ? '+ ' : '';

        $user = $this->user;

        if (!empty($siteId)) {
            $site = $user->sites()->findOrFail($siteId);
        }

        $managerCounts = [];

        if ($user->isChatPaid()) {
            $minOperators = $user->chat_paid_managers_count === Chat::getUnlimitFrom() 
                ? $user->chat_paid_managers_count
                : $user->chat_paid_managers_count + 1;

            $boughtCount = $user->chat_paid_managers_count;
            $availableCount -= $boughtCount;
        } else {
            // менеджеры ещё не оплачены, первая покупка
            $minOperators = 1;
            $availableCount = count(Chat::getDiscountGrid());
            $boughtCount = 0;
        }

        foreach (range(1, $availableCount) as $i) {
            if ($i === (Chat::getUnlimitFrom() - $boughtCount)) {
                $managerCounts[$i+$boughtCount] = "без ограничений";
            } else {
                $managerCounts[$i+$boughtCount] = $countSelectPrefix . "{$i} ". plural($i, ['оператор', 'операторов', 'оператора']);
            }
        }
        if (empty($managerCounts)) {
            $managerCounts[Chat::getUnlimitFrom()] = "без ограничений";
        }

        switch ($mode) {
            case 'period':
                $modalTitle = "Продлить тариф";
                $view = view("user.managers.modal.extend-chat-period", compact('user'));
                break;
            case 'free':
                $modalTitle = "Добавить бесплатного оператора";
                $view = view("user.managers.modal.buy-chat-seats-free", compact('managerCounts','user','operatorNo'));
                break;
            case 'seats':
                $modalTitle = "Добавить операторов";
                $view = view("user.managers.modal.extend-chat-seats", compact('managerCounts','user','operatorNo'));
                break;
            default:
                $modalTitle = "Добавить операторов";
                $view = view("user.managers.modal.buy-chat-seats", compact('managerCounts', 'user','operatorNo'));
                break;
        }

        $modalContent = $view->render();
        $modalButtons = view("user.managers.modal._buy-chat-seats-buttons", compact($mode))->render();

        return $this->respond(view('common.modal-dialog', compact('modalTitle', 'modalContent', 'modalButtons')));
    }


    public function isChatPaymentRequired($siteId, $managerId)
    {
        $site = $this->user->sites()->findOrFail($siteId);
        $manager = $site->siteManagers()->findOrFail($managerId);

        if (
            !empty($site->chat)
            and $site->chat->tarif == Chat::CHAT_TARIF_OPERATOR 
            and !$manager->relation($site)->is_payed
            and $manager->pivot->chat == ManagerRelation::DISABLED
        ) {
            return $this->respond(['yes' => true]);
        }

        return $this->respond(['no' => true]);
    }

    /**
     * Создание нового менеджера
     */
    public function onCreate(Request $request)
    {
        $input = $request->all();
        $relation = $request->get('relation');
        $siteId = $request->get('siteId');
        $module = $request->get('module');
        $moduleAccess = $request->get('module-access');

        // параметры для pivot table
        $pivotParams = [];

        $site = $this->user->sites()->find($siteId);
        $manager = $this->user->newUserManager();

        $validator = Validator::make(
            $input,
            $manager->rules($module, $request->get('type'))
        );

        if ($validator->fails()) {
            return $this->respondValidation($validator->errors());
        }

        if (!empty($input['avatar'])) {
            $manager->saveAvatar($input['avatar']);
        }

        DB::beginTransaction();
        try {
            $manager->fill($input);
            if (!empty($manager->password)) {
                $manager->password = bcrypt($manager->password);
            }

            $manager->save();

            $siteIds = $this->user->sites()->get()->lists('id');
            $manager->sites()->sync($siteIds);

            if (!empty($relation)) {
                $pivotParams = collect($relation)->only(['is_settings', 'name', 'role']);

                if (isset($relation['avatar'])) {
                    $pivotParams['avatar'] = $manager->saveRelationAvatar($relation['avatar']);
                }
            }

            if (!empty($module)) { // модуль, в котором создаётся менеджер
                $pivotParams[$module] = $request->get($module.'-status');

                $manager->relation($site)->tryTurnOn($module);
                if($module == Site::SUCCESS_CALLBACK){
                    $manager->relation($site)->tryTurnOn(Site::SUCCESS_AGENT_DEFAULT);
                }
            }

            if ($site) {
                $integrations = collect(Integration::$type)->keys();

                $pivotParams['integration'] = json_encode($integrations);
                $manager->sites()->updateExistingPivot($site->id, $pivotParams);
            }

            // создаём запись в phones
            $manager->updatePhone();

            if ($moduleAccess) {
                foreach ($moduleAccess as $module => $status) {
                    $method = $status == ManagerRelation::DISABLED ? "tryDetach" : "tryAttach";
                    $manager->relation($siteId)->$method($module);
                }
            }

        } catch (Exception $e) {
            DB::rollback();
            return $this->respondError();
        }
        DB::commit();

        if (isset($paymentResult) and !$paymentResult) {
            $message = "Недостаточно средств для включения менеджера! Пожалуйста, пополните баланс!";
            return $this->respondValidation([
                'isModal' => true,
                'errorModalContent' => view('user.managers.modal.error', compact('message'))->render(),
            ]);
        }

        return $this->respond([
            'ok' => true,
            'data' => $manager->getAttributes(),
            'siteId' => $siteId,
            'balance' => $this->user->fresh()->balance
        ]);
    }


    /**
    * Удаляет менеджера
    */
    public function onDeleteManager(Request $request)
    {
        $managerId = $request->get('id');
        $manager = $this->user->userManagers()->findOrFail($managerId);

        DB::beginTransaction();

        try {
            $manager->delete();
        } catch (Exception $e) {

            DB::rollback();
            return $this->respondError();
        }

        DB::commit();

        return $this->respond(['ok' => true]);
    }

    public function onRestoreManager(Request $request){
        $managerId = $request->get('id');
        $manager = $this->user->userManagers()->withTrashed()->findOrFail($managerId);
        try {
            $manager->restore();
        } catch (Exception $e) {

            DB::rollback();
            return $this->respondError();
        }

        return $this->respond(['ok' => true]);
    }

    /**
     * Сохранение алиаса менеджера
     */
    public function onUpdateAlias(Request $request, $managerId, $siteId)
    {
        $input = $request->only(['name', 'avatar', 'role', 'is_settings']);

        $manager = $this->user->userManagers()->findOrFail($managerId);
        $site = $manager->sites()->findOrFail($siteId);
        $alias = $site->pivot;

        $validator = Validator::make($input, $manager->aliasRules());

        if ($validator->fails()) {
            return $this->respondValidation($validator->errors());
        }

        $path = $alias->avatar;
        
        if (!empty($input['avatar'])) {
            $path = $manager->saveRelationAvatar($input['avatar']);
        }

        $alias->update([
            'avatar' => $path,
            'name' => $input['name'],
            'role' => $input['role'],
            'is_settings' => $input['is_settings'],
        ]);

        $data = $alias->getAttributes();
        $data['changed'] = !empty($alias->name);
        $data['avatar'] = $manager->getAliasAvatarPath($site->id);
        $data['name'] = $input['name'];

        return $this->respond(['success' => true, 'data' => $data]);
    }

    /**
     * Вкл/выкл модуля для менеджера
     */
    public function onSwitchModule(Request $request)
    {
        $id      = $request->get('id');
        $site_id = $request->get('site_id');
        $module  = $request->get('module');

        $manager = $this->user->userManagers()->findOrFail($id);
        $site = $manager->sites()->findOrFail($site_id);//

        $state = $site->pivot->$module;

        try {
            switch ($state) {
                case ManagerRelation::DISABLED:
                    $manager->relation($site)->tryTurnOn($module);
                    break;
                case ManagerRelation::ENABLED:
                default:
                    $manager->relation($site)->tryTurnOff($module);
                    break;
            }
        } catch (ManagerStatusException $e) {
            return $this->respondValidation($e->getRespondArray());
        }

        return $this->respond([
            'status' => $manager->relation($site)->$module,
            'balance' => $this->user->fresh()->balance,
        ]);
    }

    /**
     * managerId, siteId
     */
    public function onDetach($module, Request $request)
    {
        $site = $this->user->sites()->findOrFail($request->get('siteId'));

        $moduleInstance = $site->getModuleInstance($module);

        if (!($moduleInstance instanceof \App\Models\Site\ManagerableInterface)) {
            throw new \Exception("Module $module does not support managers");
        }
        $manager = $site->siteManagers($module)->findOrFail($request->get('managerId'));

        DB::beginTransaction();

        try {
            $manager->relation($site)->tryDetach($module);
        } catch (ManagerStatusException $ex) {
            DB::rollback();
            return $this->respondValidation($ex->getRespondArray());
        } catch (\Exception $e) {
            DB::rollback();
            return $this->respondError($e->getMessage());
        }

        DB::commit();
        return $this->respond(['ok' => true, 'result' => '']);
    }

    /**
     * добавляет оператора в модуль
     */
    public function onAddOperator($module, Request $request)
    {
        $site = $this->user->sites()->findOrFail($request->get('siteId'));
        $manager = $site->siteManagers()->findOrFail($request->get('managerId'));

        DB::beginTransaction();
        try {
            $manager->relation($site)->tryTurnOn($module);

            if($module == Site::SUCCESS_CALLBACK){
                $manager->relation($site)->tryTurnOn(Site::SUCCESS_AGENT_DEFAULT);
            }
            $pivotParams = [
                'integration' => = json_encode(collect(Integration::$type)->keys())
            ];
            $manager->sites()->updateExistingPivot($site->id, $pivotParams);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            return $this->respondError($e->getMessage());
        } catch (ManagerStatusException $ex) {
            DB::rollback();
            return $this->respondValidation($ex->getRespondArray());
        }

        return $this->respond(['ok' => 'true']);
    }

    /**
     * возвращает данные для js dataTable
     */
    public function dtAllManagers($module, $siteId)
    {
        $site = $this->user->sites()->findOrFail($siteId);
        $managers = $site->freeManagers($module);

        $moduleInstance = null;

        if(in_array($module,$site->getAgentSite())){
            $moduleInstance = $site;
        }elseif(!empty($site->$module)){
            $moduleInstance = $site->$module;
        }

        if (!($moduleInstance instanceof \App\Models\Site\ManagerableInterface)) {
            throw new \Exception("Module $module does not support managers");
        }

        $managers->select([
            Manager::tableName().'.id',
            Manager::tableName().'.name',
            Manager::tableName().'.avatar',
        ]);

        $dt = Datatables::of($managers);
        $vars = compact('site', 'module');

        $dt->editColumn('avatar', function($manager) use($vars) {
            $vars['manager'] = $manager;
            return view('user.managers.modal._all-operators-avatar', $vars)->render();
        })->editColumn('name', function($manager) use($vars) {
            $vars['manager'] = $manager;
            return view('user.managers.modal._all-operators-name', $vars)->render();
        })->addColumn('buttons', function($manager) use($vars) {
            $vars['manager'] = $manager;
            return view('user.managers.modal._all-operators-buttons', $vars)->render();
        });

        return $this->respond($dt);
    }


    public function calculateChatPrice(Request $request)
    {
        $period = $request->get('period');
        $managers = $request->get('operators');

        if ($period === null or $managers === null) {
            return $this->respondValidation("period and managers count are required!");
        }

        $price = $this->user->calculateChatTariffPrice($period, $managers);

        $benefits = $this->user->calculateChatTariffBenefits($period, $managers);

        $ok = true;
        return $this->respond(compact('ok', 'price', 'benefits'));
    }


    /**
     * "покупка" первого бесплатного оператора
     */
    public function onBuyChatSeatsFree(Request $request)
    {
        $user = $this->user;

        $period = $request->get('period');

        DB::beginTransaction();
        try {

            if ($user->free_operator_workplaces >= \App\Models\User\User::ALLOWED_FREE_OPERATORS_WORKPLACES) {
                throw (new ManagerStatusException("Произошла ошибка! Вы у Вас уже есть бесплатный оператор."))->setModal(true);
            }

            $user->createChatSeatsFree();

            DB::commit();

            return $this->respond(['ok' => true, 'balance' => $user->balance]);

        } catch (ManagerStatusException $e) {
            DB::rollback();
            return $this->respondValidation($e->getRespondArray());
        } catch (Exception $e) {
            DB::rollback();
            return $this->respondError("Произошла ошибка! Обратитесь в техподдержку.");
        }
    }


/**
 * Создаёт места для менеджеров чата, снимает деньги с баланса юзера
 */
    public function onBuyChatSeats(Request $request)
    {
        $user = $this->user;

        $period = $request->get('period');
        $managers = $request->get('managers');
        $site = $this->user->sites()->find($request->get('siteId'));

        DB::beginTransaction();
        try {
            if ($period === null or $managers === null) {
                throw (new ManagerStatusException("Произошла ошибка! Попробуйте обновить страницу в браузере."))->setModal(true);
            }

            $price = $this->user->calculateChatTariffPrice($period, $managers);

            if ($user->balance < $price) {
                throw (new ManagerStatusException())->setLowBalance($price-$user->balance);
            }

            if (intval($period) === 0) {
                $paymentMessage = 'Подключение новых рабочих мест ('
                    . ($managers == Chat::getUnlimitFrom() ? '<b>не ограниченное количество</b>' : '<b>'. $managers . '</b> мест')
                    . ') в кабинете оператора';
            } else {
                $paymentMessage = 'Продление рабочих мест ('
                    . ($managers == Chat::getUnlimitFrom() ? '<b>не ограниченное количество</b>' : '<b>' . $managers . '</b> мест')
                    . ') в кабинете оператора на <b>'.($period*30).' дней</b>';
            }

            \App\Services\Spend::create()
                ->setAmount($price)
                ->setOperationId(Payment::WITHDRAW_BALANCE_TO_SUBSCRIPTION_CHAT)
                ->setComment($paymentMessage)
                ->process();

            $user->createChatSeats($period, $managers);

            $user->chat_autopayment = intval((bool)$request->get('autosubscription'));
            $user->save();

            DB::commit();

            return $this->respond([
                'ok' => true,
                'balance' => $user->fresh()->balance
            ]);

        } catch (ManagerStatusException $e) {
            DB::rollback();
            return $this->respondValidation($e->getRespondArray());
        } catch (Exception $e) {
            DB::rollback();
            return $this->respondError("Произошла ошибка! Обратитесь в техподдержку.");
        }
    }

    public function onToggleAutopaymentStatus(Request $request, $userId)
    {
        $user = $this->user;

        if ($user->id != $userId) {
            return $this->respondError('Произошла ошибка.');
        }

        $user->chat_autopayment = abs($user->chat_autopayment - 1);
        $user->save();

        return $this->respond(['ok' => true]);
    }
}
